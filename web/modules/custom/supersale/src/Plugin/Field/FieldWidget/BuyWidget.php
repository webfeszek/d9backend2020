<?php

namespace Drupal\supersale\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the 'supersale_buy' field widget.
 *
 * @FieldWidget(
 *   id = "supersale_buy",
 *   label = @Translation("Buy"),
 *   field_types = {
 *      "supersale_buy",
 *      "supersale_buywithstock"
 *    },
 * )
 */
class BuyWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['price'] = [
      '#type' => 'number',
      '#default_value' => isset($items[$delta]->price) ? $items[$delta]->price : NULL,
      '#step' => 0.1,
    ];

    $element['#theme_wrappers'] = ['container', 'form_element'];
    $element['#attributes']['class'][] = 'container-inline';
    $element['#attributes']['class'][] = 'supersale-buy-elements';
    $element['#attached']['library'][] = 'supersale/supersale_buy';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return isset($violation->arrayPropertyPath[0]) ? $element[$violation->arrayPropertyPath[0]] : $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      if ($value['price'] === '') {
        $values[$delta]['price'] = NULL;
      }
    }
    return $values;
  }

}
