<?php

namespace Drupal\supersale\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\supersale\Form\BuyWithQuantity;

/**
 * Plugin implementation of the 'Buy With Quantity' formatter.
 *
 * @FieldFormatter(
 *   id = "supersale_buy_with_quantity",
 *   label = @Translation("Buy With Quantity"),
 *   field_types = {
 *     "supersale_buy",
 *     "supersale_buywithstock"
 *   }
 * )
 */
class BuyWithQuantityFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $entity = $items->getEntity();

    foreach ($items as $delta => $item) {

      if ($item->price) {
        $element[$delta]['price'] = [
          '#type' => 'item',
          '#markup' => $item->price . ' Ft.',
        ];
        $form = \Drupal::formBuilder()->getForm(BuyWithQuantity::class, [
          'id' => $entity->id(),
          'EntityType' => $entity->getEntityType()->getOriginalClass(),
          'title' => $entity->title->value,
          'price' => $item->price,
        ]);
        $element[$delta]['buy'] = $form;
      }

    }

    return $element;
  }

}
