<?php

namespace Drupal\supersale\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for SuperSale routes.
 */
class SupersaleController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
