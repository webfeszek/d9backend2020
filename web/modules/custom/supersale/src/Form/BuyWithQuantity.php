<?php

namespace Drupal\supersale\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\supersale\Order;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a SuperSale form.
 */
class BuyWithQuantity extends FormBase {
  /** @var Order */
  protected $order;

  public function __construct(Order $order)
  {
    $this->order = $order;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('supersale.order'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'supersale_buy_with_quantity';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['quantity'] = [
      '#type' => 'textfield',
      '#default_value' => 1,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Buy'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (mb_strlen($form_state->getValue('quantity')) == 0) {
      $form_state->setErrorByName('quantity', $this->t('Quantity should be at least 1 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $pluszData = $form_state->getBuildInfo();
    $data = $pluszData['args'][0];
    $this->order->order($data['id'], $data['EntityType'], $data['price'], $form_state->getValue('quantity'));
    $form_state->setRedirect('<front>');
  }

}
